package com.softlab.remittask.base;

/**
 * Created by olehsalamakha on 9/17/17.
 */

public interface IBasePresenter<T extends IBaseView> {
    void setView(T view);
}

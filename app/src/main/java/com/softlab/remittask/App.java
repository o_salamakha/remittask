package com.softlab.remittask;

import android.app.Application;

import com.softlab.remittask.maindi.AppComponent;
import com.softlab.remittask.maindi.AppModule;
import com.softlab.remittask.maindi.DaggerAppComponent;
import com.softlab.remittask.maindi.NetworkModule;
import com.softlab.remittask.movies.MovieComponent;
import com.softlab.remittask.movies.MovieModule;

/**
 * Created by olehsalamakha on 9/17/17.
 */

public class App extends Application {

    private AppComponent mAppComponent;
    private MovieComponent mMovieComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();

    }

    private void initAppComponent() {
        //Initialize app component
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .networkModule(new NetworkModule(getResources().getString(R.string.server_url)))
                .build();
    }


    public MovieComponent getMovieComponent() {

        if (mMovieComponent == null) {
            mMovieComponent = mAppComponent.plus(new MovieModule());
        }

        return mMovieComponent;
    }

    public void releaseMovieComponent() {
        mMovieComponent = null;
    }

    public AppComponent getComponent() {
        return mAppComponent;
    }


}

package com.softlab.remittask.restapi;

import com.softlab.remittask.entities.Categories;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by olehsalamakha on 9/17/17.
 */

/*
    Get data from the web
 */

public interface RestApi {

    @GET("videos-enhanced-c.json")
    Single<Categories> getCategories();

}

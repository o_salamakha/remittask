package com.softlab.remittask.movies;

import com.softlab.remittask.base.IBasePresenter;
import com.softlab.remittask.base.IBaseView;

import java.util.List;

/**
 * Created by olehsalamakha on 9/17/17.
 */

/*
    Contract between view and presenter
 */

public interface MovieContract {

    interface IMoviePresenter extends IBasePresenter<IMovieView> {
        void onResume();
        void onRefresh();
    }

    interface IMovieView extends IBaseView {
        void showProgress();
        void hideProgress();
        void showMovies(List<VideoItem> items);
        void showRefreshView();
        void hideRefreshView();
        void hideRefreshAnimation();
        void hideList();
        void showList();

    }
}

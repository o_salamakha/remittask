package com.softlab.remittask.movies;

import com.softlab.remittask.maindi.FragmentScope;

import dagger.Subcomponent;

/**
 * Created by olehsalamakha on 9/17/17.
 */


/*
    Subcomponent of AppComponent for MovieFragment
 */
@Subcomponent(modules = MovieModule.class)
@FragmentScope
public interface MovieComponent {
    void inject(MovieFragment fragment);
}

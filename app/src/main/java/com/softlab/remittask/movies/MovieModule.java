package com.softlab.remittask.movies;

import com.softlab.remittask.maindi.FragmentScope;
import com.softlab.remittask.restapi.RestApi;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by olehsalamakha on 9/17/17.
 */

/*
    Module provide presetner for MovieFragment
 */
@Module
public class MovieModule {
    private  MoviePresenter moviePresenter;


    public MovieModule() {

    }

    @Provides
    @FragmentScope
    public MoviePresenter providePresenter(RestApi api) {
        return new MoviePresenter(api);
    }
}

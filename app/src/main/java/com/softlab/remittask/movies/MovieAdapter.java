package com.softlab.remittask.movies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.softlab.remittask.R;

import java.util.List;

/**
 * Created by pein on 18.09.17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {

    private List<VideoItem> mItems;
    // Store the context for easy access
    private Context mContext;
    // Is used for glide
    private String baserUrl;

    public MovieAdapter(Context context, List<VideoItem> items) {
        mContext = context;
        mItems = items;
    }
    @Override
    public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_video, parent, false);

        // Return a new holder instance
        MovieHolder viewHolder = new MovieHolder(contactView);

        baserUrl = mContext.getString(R.string.server_url);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MovieHolder holder, int position) {
        VideoItem item = mItems.get(position);
        holder.mStudioTextView.setText(item.getStudio());
        holder.mTitleTextView.setText(item.getTitle());
        Glide.with(holder.mImageView).load(baserUrl + item.getImageUrl()).into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class MovieHolder extends RecyclerView.ViewHolder {
        public TextView mTitleTextView;
        public TextView mStudioTextView;
        public ImageView mImageView;

        public MovieHolder(View itemView) {
            super(itemView);

            mTitleTextView = itemView.findViewById(R.id.title_textviwe);
            mStudioTextView = itemView.findViewById(R.id.studio_textview);
            mImageView = itemView.findViewById(R.id.image_view);
        }
    }
}

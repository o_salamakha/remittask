package com.softlab.remittask.movies;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.softlab.remittask.App;
import com.softlab.remittask.R;
import com.softlab.remittask.restapi.RestApi;

import java.util.List;

import javax.inject.Inject;


public class MovieFragment extends Fragment implements MovieContract.IMovieView {

    private static final String TAG = "MovieFragment";
    private static final String SCROLL_POSITION = "SCROOL_POSITION";

    private ProgressBar mProgress;
    private RecyclerView mRecyclerListView;
    private Button mRefreshButton;
    private ViewGroup mRefreshContainer;
    private RecyclerView.LayoutManager mManager;
    private MovieAdapter mAdapter;
    private SwipeRefreshLayout mSwipeView;


    @Inject
    MoviePresenter mPresenter;

    public MovieFragment() {


    }


    private void setupMovieComponent() {
        App app = (App) (getActivity().getApplication());
        app.getMovieComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupMovieComponent();
        Log.d(TAG, "on create");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie, container, false);

        mProgress =  view.findViewById(R.id.progress_bar);
        mRefreshButton = view.findViewById(R.id.refresh_button);

        mRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.onRefresh();
            }
        });
        mRefreshContainer = view.findViewById(R.id.error_view);
        mRecyclerListView =  view.findViewById(R.id.video_recycler_view);
        mManager = new LinearLayoutManager(getActivity());
        mRecyclerListView.setLayoutManager(mManager);
        mSwipeView = view.findViewById(R.id.swiperefresh);

        mSwipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.onRefresh();
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.setView(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "on saved instance");
        outState.putParcelable(SCROLL_POSITION, mManager.onSaveInstanceState());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d(TAG, "on view state restored");
        if (savedInstanceState != null) {
            mManager.onRestoreInstanceState(savedInstanceState.getParcelable(SCROLL_POSITION));

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "on resume");
        mPresenter.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "on pause");
    }

    @Override
    public void showProgress() {
        Log.d(TAG, "show progress");
        mProgress.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMovies(List<VideoItem> items) {
        mProgress.setVisibility(View.GONE);
        mRefreshContainer.setVisibility(View.GONE);
        mRecyclerListView.setVisibility(View.VISIBLE);
        mAdapter = new MovieAdapter(getActivity(), items);
        mRecyclerListView.setAdapter(mAdapter);

        Log.d(TAG, "show movies");
    }

    @Override
    public void showRefreshView() {
        mRefreshContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRefreshView() {
        mRefreshContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideRefreshAnimation() {
        mSwipeView.setRefreshing(false);
    }

    @Override
    public void hideList() {
        mRecyclerListView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showList() {
        mRecyclerListView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        App app = (App) (getActivity().getApplication());
        app.releaseMovieComponent();
    }
}

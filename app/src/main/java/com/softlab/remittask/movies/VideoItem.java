package com.softlab.remittask.movies;

/**
 * Created by olehsalamakha on 9/17/17.
 */

/*
    POJO class which will be used in recyclerview
 */

public class VideoItem {
    private String mImageUrl;
    private String mTitle;
    private String mStudio;


    public VideoItem(String imageUrl, String title, String studio) {
        mImageUrl = imageUrl;
        mTitle = title;
        mStudio = studio;
    }


    public String getTitle() {
        return mTitle;
    }

    public String getStudio() {
        return mStudio;
    }

    public String getImageUrl() {
        return mImageUrl;
    }
}

package com.softlab.remittask.movies;

import android.util.Log;

import com.softlab.remittask.entities.Categories;
import com.softlab.remittask.entities.Video;
import com.softlab.remittask.restapi.RestApi;

import java.util.List;


import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by olehsalamakha on 9/17/17.
 */

public class MoviePresenter implements MovieContract.IMoviePresenter {
    private static final String TAG = "MoviePresenter";

    private MovieContract.IMovieView mView;
    private RestApi mApi;



    public MoviePresenter(RestApi api) {
        this.mApi = api;
    }

    @Override
    public void setView(MovieContract.IMovieView view) {
        Log.d(TAG, "set view");
        mView = view;
    }

    @Override
    public void onResume() {
            mView.showProgress();
            fetchData();
    }


    @Override
    public void onRefresh() {
        mView.hideRefreshView();
        mView.showProgress();
        fetchData();
    }



    private void fetchData() {
        mApi.getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Categories>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Categories value) {

                        List<Video> videoList = value.getCategories().get(0).getVideos();
                        //map Video to VideoItem it will be easy to use in View
                        Observable.fromIterable(videoList)
                                .map(new Function<Video, VideoItem>() {
                                    @Override
                                    public VideoItem apply(Video video) throws Exception {
                                        return new VideoItem(video.getImage480x270(),
                                                video.getTitle(), video.getStudio());
                                    }
                                }).toList().doOnSuccess(new Consumer<List<VideoItem>>() {
                            @Override
                            public void accept(List<VideoItem> videoItems) throws Exception {
                                mView.hideProgress();
                                mView.hideRefreshAnimation();
                                mView.showList();
                                mView.showMovies(videoItems);
                            }
                        }).subscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                        mView.hideRefreshAnimation();
                        mView.hideList();
                        mView.showRefreshView();
                    }
                });
    }
}

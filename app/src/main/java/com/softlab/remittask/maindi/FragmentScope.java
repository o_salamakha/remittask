package com.softlab.remittask.maindi;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;
/**
 * Created by olehsalamakha on 9/17/17.
 */


/*
    Scope which will exists only in special fragment
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentScope {
}

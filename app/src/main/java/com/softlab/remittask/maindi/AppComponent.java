package com.softlab.remittask.maindi;

import com.softlab.remittask.MainActivity;
import com.softlab.remittask.movies.MovieComponent;
import com.softlab.remittask.movies.MovieModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by olehsalamakha on 9/17/17.
 */

/*
    MainComponent of di it is a parent component for MovieComponent
 */
@Component(modules = {AppModule.class, NetworkModule.class})
@Singleton
public interface AppComponent {

    //Movive component is a subcomponent and should be added here
    MovieComponent plus(MovieModule module);
}
